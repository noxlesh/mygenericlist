﻿using System;
using MyCollections;

namespace Demo.MyGeneric
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDictionary<string, string> dict = new MyDictionary<string, string>();
            MyDictionary<string, string> dict1 = new MyDictionary<string, string>();
            string[] english = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
            string[] russian = {"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять"};
            for(int i = 0;i < english.Length;i++)
            {
                dict.Add(russian[i], english[i]);                  
            }

            string s = dict.SerializeStr();
            dict1.DeserializeStr(s);
            string t = dict1.SerializeStr();
            
        }
    }
}
