﻿using System.Collections.Generic;
using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace MyCollections
{
    
    public class MyDictionary<TKey, TValue> : IDictionary<TKey, TValue> where TKey:IComparable
    {
        /// <summary>
        /// A binary tree node
        /// </summary>
        private class Node
        {
            public KeyValuePair<TKey, TValue> pair;
            public Node left;
            public Node right;

            public Node(KeyValuePair<TKey, TValue> pair)
            {
                this.pair = pair;
            }
        }

        private Node _root;
        private int _count;
        
        /// <summary>
        /// Delegate used in the FindNode to process each item 
        /// </summary>
        /// <param name="kv">An item to process.</param>
        public delegate void ProcessPair(KeyValuePair<TKey, TValue> kv);
        
        /// <summary>
        /// An item enumerator
        /// </summary>
        /// <returns>Current item</returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            using (IEnumerator<Node> e = GetNodeEnumerator())
            {
                while (e.MoveNext())
                {
                    yield return e.Current.pair;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Internal node enumerator
        /// </summary>
        /// <returns>Current value</returns>
        private IEnumerator<Node> GetNodeEnumerator()
        {
            Node node = _root;
            Stack<Node> stack = new Stack<Node>();
            while (node != null || stack.Count != 0)
            {
                if (stack.Count != 0)
                {
                    node = stack.Pop();

                    yield return node;

                    if (node.right != null)
                    {
                        node = node.right;
                    }
                    else
                    {
                        node = null;
                    }
                }

                while (node != null)
                {
                    stack.Push(node);
                    node = node.left;
                }
            }
        }
        
        /// <summary>
        /// Adds a new item
        /// </summary>
        /// <param name="item">A new Key Value pair</param>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            if(FindNode(item.Key) != null) 
                throw new ArgumentException("Item with similar key already exists in the dictionary!");
            if (_root == null)
            {
                _root = new Node(item);
                _count++;
            }
            else
            {
                InsertRecursive(item, _root);
            }
        }

        


        /// <summary>
        /// Clears a dictionary
        /// </summary>
        public void Clear()
        {
            _root = null;
            _count = 0;
            //TODO: is there need to clear the references of the nodes?
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if(array == null) throw new ArgumentNullException("Array is not initialized!");
            if(arrayIndex < 0 || arrayIndex >= array.Length) throw new ArgumentOutOfRangeException("Array index is out of range!");
            int availableArraySpace = array.Length - arrayIndex;
            if (_count > availableArraySpace)
                throw new ArgumentException("Space of array after index is not enough!");
            ForEach(pair => array[arrayIndex++] = pair);
        }

        
        /// <summary>
        /// Checks if an item exists in a dictionary
        /// </summary>
        /// <param name="item">An item to check</param>
        /// <returns>True if found and false otherwise.</returns>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            Node node = FindNode(item.Key);
            if (node != null)
            {
                if (node.pair.Equals(item)) return true;
            }
            return false;
        }

        /// <summary>
        /// Deletes an item from a dictionary
        /// </summary>
        /// <param name="item">An item to delete</param>
        /// <returns>True if deletion was successfull and false oterwise.</returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return Remove(item.Key);
        }

        /// <summary>
        /// Returns an items count
        /// </summary>
        public int Count => _count;
        
        /// <summary>
        /// The readonly feature wasnt implemented
        /// </summary>
        public bool IsReadOnly => false;
        
        /// <summary>
        /// Adds a new item
        /// </summary>
        /// <param name="key">Key for a new item</param>
        /// <param name="value">Value for a new item</param>
        public void Add(TKey key, TValue value)
        {
            if(key == null) throw new ArgumentNullException("Кеу parameter is null!");
            Add(new KeyValuePair<TKey, TValue>(key, value));
        }
        
        /// <summary>
        /// Checks is key is presented in a dictionary
        /// </summary>
        /// <param name="key">A key to operate on.</param>
        /// <returns>True if a key was finded and false otherwise</returns>
        public bool ContainsKey(TKey key)
        {
            return FindNode(key) != null;
        }

        /// <summary>
        /// Deletes an item from a dictionary by a key
        /// </summary>
        /// <param name="key">A key of an item to delete</param>
        /// <returns>True if deletion was successfull and false oterwise.</returns>
        public bool Remove(TKey key)
        {
            Node node = FindNode(key);
            if (node == null) return false;
            if (node.left == null && node.right == null) // first case delete leaf
            {
                node = null;
            }
            else if (node.left == null) // second case delete one child node
            {
                node = node.right;
            } 
            else if (node.right == null)
            {
                node = node.left;
            }
            else // third case delete tow child node
            {
                Node tmp;
                if (node.right.left != null)
                {
                    tmp = node.right.left;
                    Node tmpPrnt = node.right;
                    while (tmp.left != null)
                    {
                        tmpPrnt = tmp;
                        tmp = tmp.left;
                    }
                    node.pair = tmp.pair;
                    if (tmp.right != null)
                    {
                        tmpPrnt.left = tmp.right;
                    }
                }
                else
                {
                    tmp = node.right;
                    node.pair = tmp.pair;
                    if (tmp.right != null)
                        node.right = tmp.right;
                }  
            }

            _count--;
            return true;
        }
    
        /// <summary>
        /// Try to get a value by a key
        /// </summary>
        /// <param name="key">Key to find value</param>
        /// <param name="value">A value which returns if a key was finded</param>
        /// <returns>True if key was finded and false otherwise</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            Node n = FindNode(key);
            if (n != null)
            {
                value = n.pair.Value;
                return true;
            }
            value = default(TValue);
            return false;
      }
        
        /// <summary>
        /// Getter and setter of a value by a key
        /// </summary>
        /// <param name="key">Intended to find a node</param>
        public TValue this[TKey key] {
            get
            {
                Node n = FindNode(key);
                if(n == null) throw new ArgumentOutOfRangeException();
                return FindNode(key).pair.Value;
            }
            set
            {
                Node n = FindNode(key);
                if (n == null)
                {
                    Add(new KeyValuePair<TKey, TValue>(key, value));
                }
                else
                {
                    FindNode(key).pair = new KeyValuePair<TKey, TValue>(key, value);
                }
                
            }

        }

        /// <summary>
        /// Returns a collection of keys
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                ICollection<TKey> keys = new List<TKey>();
                ForEach(pair => keys.Add(pair.Key));
                return keys;
            }
            
        }
        
        /// <summary>
        /// Returns a collection of values
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                ICollection<TValue> vals = new List<TValue>();
                ForEach(pair => vals.Add(pair.Value));
                return vals;
            }
           
        }
        
        /// <summary>
        /// Finds a node by key and returns it.
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>Node which is suggested by key</returns>
        private Node FindNode(TKey key)
        {
            if (_root == null) return null;
            Node node = _root;
            while (true)
            {
                if (node.pair.Key.CompareTo(key) > 0) // traverse to left
                {
                    if(node.left != null)
                        node = node.left;
                    else
                        return null;
                } 
                else if (node.pair.Key.CompareTo(key) < 0) // traverse to right 
                {
                    if (node.right != null)
                        node = node.right;
                    else
                        return null;
                }
                else if( node.pair.Key.CompareTo(key) == 0) // equals
                {
                    return node;
                }
            }
            
        }
        
        /// <summary>
        /// Helper for insertion of a new item
        /// </summary>
        /// <param name="item">An item to insert</param>
        /// <param name="parent">Previous node</param>
        /// <exception cref="ArgumentException">Throws if an item already exists.</exception>
        private void InsertRecursive(KeyValuePair<TKey, TValue> item, Node parent)
        {
            if(parent.pair.Key.CompareTo(item.Key) == 0) 
                throw new ArgumentException($"Key [{item.Key}] already exists!");
            
            if (parent.pair.Key.CompareTo(item.Key) > 0) // traverse to left
            {
                if (parent.left == null)
                {
                    parent.left = new Node(item);
                    _count++;
                }
                else
                {
                    InsertRecursive(item, parent.left);
                }
                
            }
            else // traverse to right
            {
                if(parent.right == null)
                {
                    parent.right = new Node(item);
                    _count++;
                }
                else
                {
                    InsertRecursive(item, parent.right);
                }   
            }
        }

        /// <summary>
        /// Serialize a BSTDIctionary object to a string
        /// </summary>
        /// <returns>Serialized string</returns>
        public string SerializeStr()
        {
            var serializedStr = new StringBuilder();
            var queue = new Queue<Node>();
            if(_root != null) queue.Enqueue(_root);

            while (queue.Any())
            {
                Node item = queue.Dequeue();
                serializedStr.Append(item == null ? "#\n" : $"{item.pair.Key} {item.pair.Value}\n");
                if (item != null)
                {
                    queue.Enqueue(item.left);
                    queue.Enqueue(item.right);
                }
            }

            return serializedStr.ToString();
        }
        
        /// <summary>
        /// Deseerialize a string to a BSTDIctionary object 
        /// </summary>
        /// <param name="str">Serialized string</param>
        public void DeserializeStr(string str)
        {
            string[] ss = str.Split('\n');
            if (ss[0] == "#") return;
            var sp = ss[0].Split(' ');
            var queue = new Queue<Node>();
            _root = new Node(new KeyValuePair<TKey, TValue>((TKey)Convert.ChangeType(sp[0], typeof(TKey)), (TValue)Convert.ChangeType(sp[1], typeof(TValue))));
            queue.Enqueue(_root);
            _count = 1;
            int index = 1;
            while (queue.Any())
            {
                var size = queue.Count;
                for (int i = 0; i < size; i++)
                {
                    var node = queue.Dequeue();
                    var line = ss[index];
                    if (line == "#")
                    {
                        node.left = null;
                    }
                    else
                    {
                        sp = line.Split(' ');
                        node.left = new Node(new KeyValuePair<TKey, TValue>((TKey)Convert.ChangeType(sp[0], typeof(TKey)), (TValue)Convert.ChangeType(sp[1], typeof(TValue))));
                        queue.Enqueue(node.left);
                        _count++;
                    }
                    index++;
                    line = ss[index];
                    if (line == "#")
                    {
                        node.right = null;
                    }
                    else
                    {
                        sp = line.Split(' ');
                        node.right = new Node(new KeyValuePair<TKey, TValue>((TKey)Convert.ChangeType(sp[0], typeof(TKey)), (TValue)Convert.ChangeType(sp[1], typeof(TValue))));
                        queue.Enqueue(node.right);
                        _count++;
                    }
                    index++;
                }
            }
        }
        
        /// <summary>
        /// Process each pair in the tree by delegate
        /// </summary>
        /// <param name="processPair">Delegate has current iteration pair as parameter to process it.</param>
        public void ForEach(ProcessPair processPair)
        {
            using (IEnumerator<Node> e = GetNodeEnumerator())
            {
                while (e.MoveNext())
                {
                    processPair(e.Current.pair);
                }
            }

        }
    }
}