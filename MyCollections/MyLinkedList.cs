﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MyCollections
{
    /// <summary>
    /// Demo class impliments LinkedList
    /// </summary>
    /// <typeparam name="T">Generic type</typeparam>
    public class MyLinkedList<T> : IList<T>, IEnumerator<T>
    {
        #region inner classes
        private class ListItem
        {
            public T _value;
            public ListItem _next;

            public ListItem(T value, ListItem next = null)
            {
                _value = value;
                _next = next;
            }
        }
        #endregion
        #region fields
        private ListItem _head = null;
        private ListItem _tail = null;
        private int _counter = 0;
        private ListItem _currentItem = null;
        #endregion

        #region ctors

        /// <summary>
        /// Default constructor
        /// </summary>
        public MyLinkedList()
        {            
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="array">for initialization initial data</param>
        public MyLinkedList(IEnumerable<T> array)
        {            
            using (IEnumerator<T> e = array.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    Add(e.Current);
                }
            }
        }
        #endregion

        #region properties and indexer
        public T this[int index]
        {
            get => GetItem(index)._value;
            set => GetItem(index)._value = value;
        }

        public int Count { get => _counter; }

        public bool IsReadOnly { get => false; }

        #endregion

        #region utilites
        private ListItem GetItem(int index)
        {
            if (index < 0 || index >= _counter) throw new ArgumentOutOfRangeException("Index is out of range!");
            ListItem item = _head;
            while(index-- > 0)
            {
                item = item._next;
            }
            return item;
        }
        #endregion

        #region methods

        /// <summary>
        /// Add to Head
        /// </summary>
        /// <param name="val">Generic type, additional value</param>
        public void AddHead(T val)
        {
            if(_tail==null && _head == null)
            {
                _head = _tail = new ListItem(val);
                _counter = 1;
            }
            else
            {
                _head = new ListItem(val, _head);
                ++_counter;
            }
        }
        /// <summary>
        /// Add to Tail
        /// </summary>
        /// <param name="val">Generic type, additional value</param>
        public void Add(T val)
        {
            if (_tail == null && _head == null)
            {
                _head = _tail = new ListItem(val);
                _counter = 1;
            }
            else
            {
                _tail = _tail._next = new ListItem(val);
                ++_counter;
            }
        }

        public void Clear()
        {            
            _counter = 0;
            _head = _tail = null;
        }

        public bool Contains(T val)
        {
            using(IEnumerator<T> e = GetEnumerator())
            {
                while (e.MoveNext())
                {
                    if(e.Current.Equals(val))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null) throw new ArgumentNullException("Array is not initialized!");
            if (arrayIndex < 0 || arrayIndex >= array.Length)
                throw new ArgumentOutOfRangeException("Array index is out of range!");
            int availableArraySpace = array.Length - arrayIndex;
            if (_counter > availableArraySpace)
                throw new ArgumentException("Space of array after index is not enough!");
            using (IEnumerator<T> e = GetEnumerator())
            {
                while (e.MoveNext())
                {
                    array[arrayIndex++] = e.Current;
                }
            }
        }
       
        public int IndexOf(T val)
        {
            ListItem item = _head;
            for(int i=0; item!=null; ++i, item = item._next)
            {
                if (item._value.Equals(val))
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, T val)
        {
            if(index > _counter) throw new ArgumentOutOfRangeException("Index is bigger than counter");
            if (index == 0)
            {
                AddHead(val);
                return;
            }
            if (index == _counter)
            {
                Add(val);
                return;
            }
            ListItem item = GetItem(index - 1);
            item._next = new ListItem(val, item._next);
            ++_counter;
        }       

        public bool Remove(T val)
        {
            if (_head == null) return false;
            if (_head._value.Equals(val))
            {
                _head = _head._next;
                --_counter;
                return true;
            }
            ListItem item = _head;
            while(item._next!=null)
            {
                if (val.Equals(item._next._value))
                {
                    if (item._next._next == null)
                        _tail = item._next;
                    else
                        item._next = item._next._next;
                    --_counter;
                    return true;
                }
                item = item._next;
            }
            return false;
            
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= _counter) throw new ArgumentOutOfRangeException("Index is out of range!");
            if (_head == null) return; // return if list is empty
            if (index == 0)
            {
                _head = _head._next;
                _counter--;
            }
            else
            {
                ListItem prev_item = GetItem(index-1);
                if (index != _counter - 1)
                {
                    prev_item._next = prev_item._next._next;
                } else
                {
                    _tail = prev_item;
                    _tail._next = null;
                }
                _counter--;
            }
        }
        #endregion

        #region IEnumerator 

        public T Current { get => _currentItem._value; }
        object IEnumerator.Current { get => Current; }
                
        public void Dispose()
        {
            Reset();
        }        
        public void Reset()
        {
            _currentItem = null;
        }
        public bool MoveNext()
        {
            if (_currentItem == null)
            {
                _currentItem = _head;
            }
            else
            {
                _currentItem = _currentItem._next;
            }
            return _currentItem != null;
        }
        #endregion
        #region IEnumerable
        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
       
    }
}
