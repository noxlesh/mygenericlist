﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace MyCollections
{
    public sealed class MyList<T> : IList<T>, IEnumerator<T>
    {
        #region fields
        private T[] _arr;
        private int _count;
        private int _current_pos;
        #endregion

        #region properties and indexator
        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= _count) throw new ArgumentOutOfRangeException("Index is out of range!");
                return _arr[index];
            }
            set
            {
                if (index < 0 || index >= _count) throw new ArgumentOutOfRangeException("Index is out of range!");
                _arr[index] = value;
            }
        }

        public int Count => _count;

        public bool IsReadOnly => false;

        public T Current => _arr[_current_pos];

        object IEnumerator.Current => _arr[_current_pos];
        #endregion

        #region ctors
        public MyList(int capacity = 10)
        {
            _arr = new T[capacity];
            _count = 0;
            _current_pos = -1;
        }
        #endregion


        #region public methods
        public void Add(T value)
        {
            IncreaseArrOndemand();
            _arr[_count++] = value;
        }

        public void Clear()
        {
            _count = 0;
        }

        public bool Contains(T item)
        {
            for (int i = 0; i < _arr.Length; i++)
            {
                if (EqualityComparer<T>.Default.Equals(item, _arr[i]))
                    return true;
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null) throw new ArgumentNullException("Array is not initialized!");
            if (arrayIndex < 0 || arrayIndex >= array.Length)
                throw new ArgumentOutOfRangeException("Array index is out of range!");
            int availableArraySpace = array.Length - arrayIndex;
            if (_count > availableArraySpace)
                throw new ArgumentException("Space of array after index is not enough!");
            for (int i = 0; i < _count; i++)
            {
                array[i + arrayIndex] = _arr[i];
            }
        }

        public int IndexOf(T item)
        {
            for (int i = 0; i < _arr.Length; i++)
            {
                if (EqualityComparer<T>.Default.Equals(item, _arr[i]))
                    return i;
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            if (index < 0 || index >= _count) throw new ArgumentOutOfRangeException("Index is out of range!");
            if (index == _count - 1)
            {
                Add(item);
            }
            else
            {
                _count++;
                IncreaseArrOndemand();
                for (int i = _count-1; i >= index; i--)
                {
                    _arr[i] = _arr[i - 1];
                }
                _arr[index] = item;
            }
        }

        public bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index != -1)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= _count) throw new ArgumentOutOfRangeException("Index is out of range!");
            for(int i = index;i < _count - 1; ++i)
            {
                _arr[i] = _arr[i + 1];
            }
            _count--;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        public void Dispose()
        {
            Reset();
        }

        public bool MoveNext()
        {
            _current_pos++;
            if (_current_pos >= _count) return false;
            return true;
        }

        public void Reset()
        {
            _current_pos = -1;
        }
        #endregion

        #region private methods
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void IncreaseArrOndemand()
        {
            if (_count == _arr.Length)
            {
                T[] tmp = new T[_arr.Length * 2];
                for (int j = 0; j < _arr.Length; ++j)
                {
                    tmp[j] = _arr[j];
                }
                _arr = tmp;
            }
        }
        #endregion
    }
}


