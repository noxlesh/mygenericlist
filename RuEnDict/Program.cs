﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using MyCollections;

namespace RuEnDict
{
    internal class Program
    {
        private const string filename = "dict.bin";
        private static MyDictionary<string, string> transDict; 
        
        public static void Main(string[] args)
        {
            transDict = new MyDictionary<string, string>();
            ReadFromFile();
            ChooseOperation();
        }

        /// <summary>
        /// Shows messages and tries to read command by user input.
        /// If input is wrong it will repeat the messages again.
        /// </summary>
        private static void ChooseOperation()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Выберите действие:");
                Console.WriteLine("1.Добавить слово-перевод.");
                Console.WriteLine("2.Получить перевод.");
                Console.WriteLine("3.Сохранить словарь и выйти.");
                int desigion = Console.Read();
                switch (desigion)
                {
                        case 49:
                            Add();
                            break;
                        case 50:
                            ShowTranslate();
                            break;
                        case 51:
                            SaveToFile();
                            return;
                }
            } 
        }

        /// <summary>
        /// Add new a word pair to a dictionary
        /// </summary>
        private static void Add()
        {
            Console.Clear();
            Console.Write("Введите русское слово и нажмите Enter: ");
            string rusWord = Console.ReadLine();
            if (!isStrCyrilic(rusWord))
            {
                ShowMessage("Слово должно сожержать только русские буквы");
                return;
            }
            Console.Clear();
            Console.Write("Введите английское слово и нажмите Enter: ");
            string engWord = Console.ReadLine();
            if (!isStrLatin(engWord))
            {
                ShowMessage("Слово должно содержать только английские буквы");
                return;
            }
            transDict.Add(new KeyValuePair<string,string>(rusWord, engWord));
        }

        /// <summary>
        /// Tries to get translation from russian to eng. Else add a new word pair to a dictionary
        /// </summary>
        private static void ShowTranslate()
        { 
            Console.Clear();
            Console.Write("Введите русское слово и нажмите Enter: ");
            string rusWord = Console.ReadLine();
            string engword;
            if (transDict.TryGetValue(rusWord, out engword))
            {
                ShowMessage($"Переводом для слова \"{rusWord}\" яаляется \"{engword}\".", 4000);
            }            else
            {
                ShowMessage($"Перевод для \"{rusWord}\" ненайден! Давайте добавим перевод.");
                Add();
            }

        }

        /// <summary>
        /// Tries to read a dictionary file to the transDict object.
        /// </summary>
        private static void ReadFromFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    string serialized = sr.ReadToEnd();
                    if(String.IsNullOrEmpty(serialized))
                        Console.WriteLine("Файл словаря пустой.");
                    else
                        transDict.DeserializeStr(serialized);
                }
            }
            catch (FileNotFoundException e)
            {
                ShowMessage("Файл словаря не найден!");
            }
            
            
        }
        
        /// <summary>
        /// Save to a dictionary file if there is any data in the transDict.
        /// </summary>
        private static void SaveToFile()
        {
            
            using (StreamWriter sw = new StreamWriter(filename))
            {
                string serialized = transDict.SerializeStr();
                if(String.IsNullOrEmpty(serialized))
                {
                    ShowMessage("Словарь пустой. Файл словаря не был создан.");
                    return;
                }               
                else
                {
                    sw.Write(serialized);
                }            }
            ShowMessage("Файл словаря сохранен.");
        }

        /// <summary>
        /// Clears console and show a message for some time
        /// </summary>
        /// <param name="str">String for message</param>
        /// <param name="msec">Milliseconds to show message. Default value is 2000 milliseconds</param>
        private static void ShowMessage(string str,int msec = 2000)
        {
            Console.Clear();
            Console.WriteLine(str);
            Thread.Sleep(msec);
        }

        /// <summary>
        /// Check string on cyrilic symbols
        /// </summary>
        /// <param name="str">String that will be checked</param>
        /// <returns>True if string has only cyrilic symbols and false otherwise</returns>
        private static bool isStrCyrilic(string str)
        {
            Regex regex = new Regex(@"[A-Za-z0-9]");
            if (regex.Match(str).Success)
                return false; // got non cyrilic and number symbols
            return true;
        }

        /// <summary>
        /// Check string on latinic symbols
        /// </summary>
        /// <param name="str">String that will be checked</param>
        /// <returns>True if string has only latinic symbols and false otherwise</returns>
        private static bool isStrLatin(string str)
        {
            Regex regex = new Regex(@"[А-Яа-я0-9]");
            if (regex.Match(str).Success)
                return false; // got non latin and number symbols
            return true;
        }
        
    }
}