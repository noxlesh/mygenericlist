﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using MyCollections;

namespace NUnitListTest
{
    [TestFixture]
    public class MyBSTDictTests
    {
        [Test]
        public void AddTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            for (int i = 0; i < 32; i++)
            {
                dict.Add(i * 2, i * 3);   
            }
            Assert.AreEqual(32, dict.Count);
        }
        
        [Test]
        public void AddKVTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            for (int i = 0; i < 32; i++)
            {
                dict.Add(new KeyValuePair<int, int>(i * 2, i * 3));   
            }
            Assert.AreEqual(32, dict.Count);
            
        }
        
        [Test]
        public void ContainsKeyTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            Random rnd = new Random();
            for (int i = 0; i < 32; i++)
            {
                dict.Add(i * 2,rnd.Next(100));
                
            }
            Assert.IsTrue(dict.ContainsKey(62));
            Assert.IsFalse(dict.ContainsKey(65));
        }

        [Test]
        public void ContainsTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            for (int i = 0; i < 32; i++)
            {
                dict.Add(i * 2, i * 3);   
            }
            Assert.IsTrue(dict.Contains(new KeyValuePair<int, int>(20, 30)));
            Assert.IsTrue(dict.Contains(new KeyValuePair<int, int>(32, 48)));
            Assert.IsFalse(dict.Contains(new KeyValuePair<int, int>(128, 324)));
        }

        [Test]
        public void RemoveByKeyTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            foreach (var i in new []{25,38,87,82,99,33,51,74,8,15,20,12})
            {
                dict.Add(i,i*2);                  
            }
            Assert.IsTrue(dict.Remove(87));
            Assert.IsTrue(dict.Remove(12));
            Assert.IsTrue(dict.Remove(51));
            Assert.IsFalse(dict.Remove(29));
            
        }

        [Test]
        public void TryGetValueTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            int val;
            foreach (var i in new []{25,38,87,82,99,33,51,74,8,15,20,12})
            {
                dict.Add(i,i*2);                  
            }
            Assert.IsTrue(dict.TryGetValue(20, out val));
            Assert.AreEqual(40,val);
            Assert.IsFalse(dict.TryGetValue(91, out val));
        }

        [Test]
        public void ClearTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            for (int i = 0; i < 32; i++)
            {
                dict.Add(i * 2, i * 3);   
            }
            dict.Clear();
            Assert.AreEqual(0, dict.Count);
        }
            
        [Test]
        public void CopyToTest()
        {
            MyDictionary<int, int>  dict = new MyDictionary<int, int>();
            KeyValuePair<int, int>[] arr = new KeyValuePair<int, int>[15];
            KeyValuePair<int, int>[] arrNull = null;
            foreach (var i in new []{25,10,38,87,82,99,33,51,74,8,15,20,12})
            {
                dict.Add(i,i*2);
            }
            Assert.DoesNotThrow(() => dict.CopyTo(arr, 2)); // must fit in arr else throw
            Assert.AreEqual(8, arr[2].Key);
            Assert.AreEqual(10, arr[3].Key);
            Assert.AreEqual(99, arr[arr.Length -1].Key);
            Assert.Throws<ArgumentNullException>(() => dict.CopyTo(arrNull, 3));
            Assert.Throws<ArgumentException>(() => dict.CopyTo(arr, 3));
            Assert.Throws<ArgumentOutOfRangeException>(() => dict.CopyTo(arr, -1));
            Assert.Throws<ArgumentOutOfRangeException>(() => dict.CopyTo(arr, arr.Length));
        }

    }
}