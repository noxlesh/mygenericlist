﻿using System;
using NUnit.Framework;
using MyCollections;

namespace NUnitListTest
{
    [TestFixture]
    public class ListTest
    {
        [Test]
        public void CtorTest()
        {
            MyList<int> list = new MyList<int>();
            Assert.AreNotEqual(null, list);
            Assert.AreEqual(0, list.Count);
            Assert.IsFalse(list.IsReadOnly);
        }

        [Test]
        public void AddTest()
        {
            MyList<int> list = new MyList<int>(4);
            Assert.AreNotEqual(null, list);
            Assert.AreEqual(0, list.Count);
            list.Add(10);
            Assert.AreEqual(1, list.Count);
            for(int i = 0; i < 100; ++i)
            {
                list.Add(i);
            }
            Assert.AreEqual(101, list.Count);
        }

        [Test]
        public void ClearTest()
        {
            MyList<int> list = new MyList<int>();
            for (int i = 0; i < 100; ++i)
            {
                list.Add(i);
            }
            Assert.AreEqual(100, list.Count); 
            list.Clear();
            Assert.AreEqual(0, list.Count);
        }

        [Test]
        public void IndexTest()
        {
            int[] arr = { 1, 2, 5, 4, 3, 7, 9 };
            MyList<int> list = new MyList<int>();
            foreach(int val in arr)
            {
                list.Add(val);
            }
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [Test]
        public void IndexSet_lessZero()
        {
            MyList<int> list = new MyList<int>();
            Assert.Throws<ArgumentOutOfRangeException>(() => list[-1] = 100);
        }

        [Test]
        public void IndexSet_gtCount()
        {
            MyList<int> list = new MyList<int>();
            Assert.Throws<ArgumentOutOfRangeException>(() => list[1] = 100);
        }

        [Test]
        public void IindexGet_lessZero()
        {
            MyList<int> list = new MyList<int>();
            int x;
            Assert.Throws<ArgumentOutOfRangeException>(() => x = list[-1]);
        }

        [Test]
        public void IndexGet_gtCount()
        {
            MyList<int> list = new MyList<int>();
            int x;
            Assert.Throws<ArgumentOutOfRangeException>(() => x = list[1]);
        }

        [Test]
        public void MoveNextTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(393);
            list.MoveNext();
            Assert.IsFalse(list.MoveNext());
        }

        [Test]
        public void ContainsTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(393);
            Assert.IsTrue(list.Contains(393));
            Assert.IsFalse(list.Contains(423));
        }

        [Test]
        public void CopyToTest_nullArray()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = null;
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            Assert.Throws<ArgumentNullException>(() => list.CopyTo(arr, 2));
        }

        [Test]
        public void CopyToTest_lessZero()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[2];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            Assert.Throws<ArgumentOutOfRangeException>(() => list.CopyTo(arr, -1));
        }

        [Test]
        public void CopyToTest_gtMax()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[2];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            Assert.Throws<ArgumentOutOfRangeException>(() => list.CopyTo(arr, 2));
        }

        [Test]
        public void CopyToTest_smallArray()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[2];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            Assert.Throws<ArgumentException>(() => list.CopyTo(arr, 1));

        }

        [Test]
        public void CopyToTest()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[10];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            list.CopyTo(arr, 2);
            Assert.AreEqual(list[0], arr[2]);
            Assert.AreEqual(list[3], arr[5]);
        }

        [Test]
        public void IndexOfTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            Assert.AreEqual(0, list.IndexOf(3193));
            Assert.AreEqual(1, list.IndexOf(4781));
            Assert.AreEqual(-1, list.IndexOf(1));
        }

        [Test]
        public void InsertTest_lessZero()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            Assert.Throws<ArgumentOutOfRangeException>(() =>list.Insert(-1, 123));
        }

        [Test]
        public void InsertTest_gtCount()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            Assert.Throws<ArgumentOutOfRangeException>(() =>list.Insert(4, 123));
        }

        [Test]
        public void InsertTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Insert(2, 194);
            Assert.AreEqual(194, list[2]);
            Assert.AreEqual(-345, list[3]);
            Assert.AreEqual(492, list[5]);
        }

        [Test]
        public void RemoveTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            Assert.IsTrue(list.Remove(4781));
            Assert.IsFalse(list.Remove(4781));
        }

        [Test]
        public void RemoveAtTest_lessZero()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            Assert.Throws<ArgumentOutOfRangeException>(() => list.RemoveAt(-1));
        }

        [Test]
        public void RemoveAtTest_gtCount()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            Assert.Throws<ArgumentOutOfRangeException>(() => list.RemoveAt(4));
        }

        [Test]
        public void RemoveAtTest_isRemovedCorrect()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(123);
            list.RemoveAt(2);
            Assert.AreEqual(-1, list.IndexOf(-345));
            Assert.AreEqual(14, list[2]);
            Assert.AreEqual(123, list[3]);
        }

    }
    
    
}