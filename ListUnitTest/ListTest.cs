﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyCollections;

namespace ListUnitTest
{
    [TestClass]
    public class ListTest
    {
        [TestMethod]
        public void CtorTest()
        {
            MyList<int> list = new MyList<int>();
            Assert.AreNotEqual(null, list);
            Assert.AreEqual(0, list.Count);
            Assert.AreEqual(false, list.IsReadOnly);
        }

        [TestMethod]
        public void AddTest()
        {
            MyList<int> list = new MyList<int>(4);
            Assert.AreNotEqual(null, list);
            Assert.AreEqual(0, list.Count);
            list.Add(10);
            Assert.AreEqual(1, list.Count);
            for(int i = 0; i < 100; ++i)
            {
                list.Add(i);
            }
            Assert.AreEqual(101, list.Count);
        }

        [TestMethod]
        public void ClearTest()
        {
            MyList<int> list = new MyList<int>();
            for (int i = 0; i < 100; ++i)
            {
                list.Add(i);
            }
            Assert.AreEqual(100, list.Count); 
            list.Clear();
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod]
        public void IndexTest()
        {
            int[] arr = { 1, 2, 5, 4, 3, 7, 9 };
            MyList<int> list = new MyList<int>();
            foreach(int val in arr)
            {
                list.Add(val);
            }
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void IndexSet_lessZero()
        {
            MyList<int> list = new MyList<int>();
            list[-1] = 100;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void IndexSet_gtCount()
        {
            MyList<int> list = new MyList<int>();
            list[1] = 100;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void IindexGet_lessZero()
        {
            MyList<int> list = new MyList<int>();
            int x = list[-1];
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void IndexGet_gtCount()
        {
            MyList<int> list = new MyList<int>();
            int x = list[1];
        }

        [TestMethod]
        public void MoveNextTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(393);
            list.MoveNext();
            Assert.AreEqual(false, list.MoveNext());
        }

        [TestMethod]
        public void ContainsTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(393);
            Assert.IsTrue(list.Contains(393));
            Assert.IsFalse(list.Contains(423));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CopyToTest_nullArray()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = null;
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            list.CopyTo(arr, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CopyToTest_lessZero()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[2];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            list.CopyTo(arr, -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CopyToTest_gtMax()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[2];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            list.CopyTo(arr, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CopyToTest_smallArray()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[2];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            list.CopyTo(arr, 1);

        }

        [TestMethod]
        public void CopyToTest()
        {
            MyList<int> list = new MyList<int>();
            int[] arr = new int[10];
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Add(194);
            list.CopyTo(arr, 2);
            Assert.AreEqual(list[2], arr[0]);
            Assert.AreEqual(list[5], arr[3]);
        }

        [TestMethod]
        public void IndexOfTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            Assert.AreEqual(0, list.IndexOf(3193));
            Assert.AreEqual(1, list.IndexOf(4781));
            Assert.AreEqual(-1, list.IndexOf(1));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void InsertTest_lessZero()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Insert(-1, 123);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void InsertTest_gtCount()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Insert(4, 123);
        }

        [TestMethod]
        public void InsertTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(492);
            list.Insert(2, 194);
            Assert.AreEqual(194, list[2]);
            Assert.AreEqual(-345, list[3]);
            Assert.AreEqual(492, list[5]);
        }

        [TestMethod]
        public void RemoveTest()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            Assert.IsTrue(list.Remove(4781));
            Assert.IsFalse(list.Remove(4781));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveAtTest_lessZero()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.RemoveAt(-1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveAtTest_gtCount()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.RemoveAt(4);
        }

        [TestMethod]
        public void RemoveAtTest_isRemovedCorrect()
        {
            MyList<int> list = new MyList<int>();
            list.Add(3193);
            list.Add(4781);
            list.Add(-345);
            list.Add(14);
            list.Add(123);
            list.RemoveAt(2);
            Assert.AreEqual(-1, list.IndexOf(-345));
            Assert.AreEqual(14, list[2]);
            Assert.AreEqual(123, list[3]);
        }

    }
}
